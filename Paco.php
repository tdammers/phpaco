<?php

require_once dirname(__FILE__) . '/common/Exceptions.php';
require_once dirname(__FILE__) . '/common/Version.php';
require_once dirname(__FILE__) . '/interpreter/Scope.php';
require_once dirname(__FILE__) . '/interpreter/Context.php';
require_once dirname(__FILE__) . '/interpreter/Interpreter.php';
require_once dirname(__FILE__) . '/interpreter/Exceptions.php';
require_once dirname(__FILE__) . '/parser/Reader.php';
require_once dirname(__FILE__) . '/parser/Parser.php';
require_once dirname(__FILE__) . '/parser/Exceptions.php';
