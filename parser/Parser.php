<?php

namespace Paco\Parser;

////// building blocks

function character(\Paco\Reader $reader, $c, $required = false) {
	if ($reader->peek() === $c) {
		return $reader->read();
	}
	else {
		$t = $reader->peek();
		if ($required)
			throw new \Paco\UnexpectedCharacterException("Unexpected '$t', expected '$c'");
		else
			throw new \Paco\NoParseException();
	}
}

function str(\Paco\Reader $reader, $str) {
	$s = $reader->read(strlen($str));
	if ($str === $s) {
		return $s;
	}
	else {
		throw new \Paco\NoParseException();
	}
}

function manyChr(\Paco\Reader $reader, $allowedChars, $allowEmpty = true) {
	if (is_string($allowedChars)) {
		$allowedChars = str_split($allowedChars);
	}
	$str = '';
	while (true) {
		if (in_array($reader->peek(), $allowedChars)) {
			$str .= $reader->read(1);
		}
		else {
			if ($allowEmpty || strlen($str)) {
				return $str;
			}
			else {
				throw new \Paco\NoParseException();
			}
		}
	}
}

function oneOf(\Paco\Reader $reader, $alternatives) {
	foreach ($alternatives as $p) {
		try {
			return $p($reader);
		}
		catch (\Paco\NoParseException $ex) {
			continue;
		}
	}
	throw new \Paco\NoParseException();
}

function many(\Paco\Reader $reader, $p) {
	$list = array();
	while (true) {
		try {
			$list[] = $p($reader);
		}
		catch (\Paco\NoParseException $ex) {
			return $list;
		}
	}
}

function manySepBy(\Paco\Reader $reader, $p, $sep, $allowTrailingSeparator = true) {
	$list = array();
	try {
		$list[] = $p($reader);
	}
	catch (\Paco\NoParseException $ex) {
		return $list;
	}

	while (true) {
		try {
			$sep($reader);
		}
		catch (\Paco\NoParseException $ex) {
			return $list;
		}
		try {
			$list[] = $p($reader);
		}
		catch (\Paco\NoParseException $ex) {
			if ($allowTrailingSeparator)
				return $list;
			else
				throw new \Paco\NoParseException();
		}
	}
}

function surroundedBy(\Paco\Reader $reader, $inner, $before, $after) {
	$before($reader);
	$result = $inner($reader);
	$after($reader);
	return $result;
}

function optional(\Paco\Reader $reader, $inner) {
	try {
		return $inner($reader);
	}
	catch (\Paco\NoParseException $ex) {
		return null;
	}
}

function braced(\Paco\Reader $reader, $inner, $openChar, $closeChar) {
	return surroundedBy($reader, $inner,
		function($r) use($openChar){return character($r, $openChar); },
		function($r) use($closeChar){return character($r, $closeChar); });
}

/////// semi-specific stuff

function whitespace_(\Paco\Reader $reader) {
	return manyChr($reader, " \t\r\n");
}

function whitespace(\Paco\Reader $reader) {
	return optional($reader, '\Paco\Parser\whitespace_');
}

function word(\Paco\Reader $reader, $firstCharPattern, $otherCharsPattern) {
	$firstChar = $reader->peek();
	if (!preg_match($firstCharPattern, $firstChar)) {
		throw new \Paco\NoParseException();
	}
	$word = $reader->read(1);
	while (preg_match($otherCharsPattern, $reader->peek())) {
		$word .= $reader->read(1);
	}
	return $word;
}

function bracedList(\Paco\Reader $reader, $inner, $openBrace, $closeBrace, $sepChar = ',') {
	return braced($reader, function($reader) use($inner, $sepChar){
					return manySepBy($reader, $inner, function($reader) use ($sepChar){
						return character($reader, $sepChar);});},
				$openBrace, $closeBrace);
}

function identifier(\Paco\Reader $reader) {
	return word($reader, '/^[a-zA-Z_]$/', '/^[a-zA-Z0-9_\\-]$/');
}

function dot(\Paco\Reader $reader) {
	return str($reader, '.');
}

function varname(\Paco\Reader $reader) {
	return oneOf($reader, array(
		'\Paco\Parser\identifier',
		'\Paco\Parser\dot'));
}

////// specific parsers - expressions

function simpleExpression_(\Paco\Reader $reader) {
	return oneOf($reader, array(
		'\Paco\Parser\variableReference',
		'\Paco\Parser\intLiteral',
		'\Paco\Parser\stringLiteral',
		'\Paco\Parser\arrayList',
		'\Paco\Parser\alistLiteral',
		'\Paco\Parser\bracedExpression',
	));
}

function simpleExpression(\Paco\Reader $reader) {
	return surroundedBy($reader, '\Paco\Parser\simpleExpression_', '\Paco\Parser\whitespace', '\Paco\Parser\whitespace');
}

function bracedExpression(\Paco\Reader $reader) {
	return braced($reader, '\Paco\Parser\expression', '(', ')');
}

function expression(\Paco\Reader $reader) {
	return binaryExpression($reader, 0);
}

function variableReference(\Paco\Reader $reader) {
	return array('getval', varname($reader));
}

function intLiteral(\Paco\Reader $reader) {
	return intval(word($reader, '/^[1-9]$/', '/^[0-9]$/'));
}

function stringLiteral(\Paco\Reader $reader) {
	$sep = $reader->peek();
	if (!in_array($sep, array('"', "'"))) {
		throw new \Paco\NoParseException();
	}
	$reader->read(1); // consume the separator
	$c = false;
	$str = "";
	while (true) {
		$c = $reader->read(1);
		switch ($c) {
			case $sep:
				return $str;
			case '\\':
				$c = $reader->read(1);
				switch ($c) {
					case 'n': $str .= "\n"; break;
					case 't': $str .= "\t"; break;
					default: $str .= $c; break;
				}
				break;
			default:
				$str .= $c;
				break;
		}
	}
}

function alistPair(\Paco\Reader $reader) {
	$key = expression($reader);
	character($reader, ':');
	$val = expression($reader);
	return array("pair", $key, $val);
}

function argumentList(\Paco\Reader $reader) {
	return bracedList($reader, '\Paco\Parser\expression', '(', ')', ',');
}

function arrayList(\Paco\Reader $reader) {
	$items = bracedList($reader, '\Paco\Parser\expression', '[', ']', ',');
	return array_merge(array("list"), $items);
}

function alistLiteral(\Paco\Reader $reader) {
	$pairs = bracedList($reader, '\Paco\Parser\alistPair', '{', '}', ',');
	return array_merge(array("alist"), $pairs);
}

/**
 * Binary operators, by precedence.
 * Each entry has two or three parts:
 * 0 - the token as found in the source
 * 1 - the s-expression head to output
 * 2 - if set and not falsy, flip the operands
 */
$binaryOperators = array(
	array(
		array("&&", "and"),
		array("||", "or"),
		array("^^", "xor"),
	),
	array(
		array("in", "elem"),
		array("contains", "elem", true),
	),
	array(
		array("==", "eq"),
		array("=", "eq~"),
		array("!==", "neq"),
		array("!=", "neq~"),
		array(">=", "gte"),
		array(">", "gt"),
		array("<=", "lte"),
		array("<", "lt"),
	),
	array(
		array("+", "add"),
		array("-", "sub"),
	),
	array(
		array("*", "mul"),
		array("/", "div"),
		array("mod", "mod"),
	),
);

function binaryExpression(\Paco\Reader $reader, $precedence) {
	global $binaryOperators;

	$maxPrecedence = count($binaryOperators);

	if ($precedence >= $maxPrecedence) {
		return unaryExpression($reader);
	}
	$lhs = binaryExpression($reader, $precedence + 1);
	while (true) {
		$reader->maybe();
		whitespace($reader);
		$token = manyChr($reader, '=!+-*/%&|^.incontas');
		$head = null;
		foreach ($binaryOperators[$precedence] as $op) {
			if ($op[0] === $token) {
				$head = $op[1];
				break;
			}
		}
		if (!$head) {
			$reader->abort();
			return $lhs;
		}
		$reader->confirm();
		$rhs = binaryExpression($reader, $precedence + 1);
		if (isset($op[2]) && $op[2])
			$lhs = array($head, $rhs, $lhs);
		else
			$lhs = array($head, $lhs, $rhs);
	}
}

function unaryExpression(\Paco\Reader $reader) {
	whitespace($reader);
	// LL(1) pre-check to avoid the overhead of speculative parsing for
	// trivial non-matches on unary "not"
	if ($reader->peek() === 'n') {
		$reader->maybe();
		try {
			str($reader, 'not');
			whitespace($reader);
			$rhs = unaryExpression($reader);
			whitespace($reader);
			$reader->confirm();
			return array("not", $rhs);
		}
		catch (\Paco\NoParseException $ex) {
			$reader->abort();
		}
	}
	$lhs = simpleExpression($reader);
	while (true) {
		switch ($reader->peek()) {
			case '(':
				$args = argumentList($reader, true);
				$lhs = array_merge(array("call", $lhs), $args);
				break;
			case '[':
				$index = braced($reader, '\Paco\Parser\expression', '[', ']');
				$lhs = array("getval", $index, $lhs);
				break;
			case '.':
				$reader->read(1);
				$index = identifier($reader);
				$lhs = array("getval", $index, $lhs);
				break;
			default:
				return $lhs;
		}
	}
}

/////// specific parsers - statements

function body(\Paco\Reader $reader) {
	$stmts = array();
	while (true) {
		$reader->maybe();
		try {
			$stmt = statement($reader);
			$stmts[] = $stmt;
			$reader->confirm();
		}
		catch (\Paco\NoParseException $ex) {
			$reader->abort();
			switch (count($stmts)) {
				case 0:
					throw new \Paco\NoParseException();
				case 1:
					return $stmts[0];
				default:
					return array_merge(array("do"), $stmts);
			}
		}
	}
}

function statement(\Paco\Reader $reader) {
	if ($reader->peek() === '{') {
		return curlyBraceStatement($reader);
	}
	else {
		return literalTextStatement($reader);
	}
}

function literalTextStatement(\Paco\Reader $reader) {
	$str = '';
	while ($reader->peek() !== '{' && !$reader->eof()) {
		$str .= $reader->read(1);
	}
	if (strlen($str) === 0) {
		$c = $reader->peek();
		throw new \Paco\NoParseException();
	}
	return array("print", $str);
}

function curlyBraceStatement(\Paco\Reader $reader) {
	if (($c = $reader->read(1)) !== '{') {
		throw new \Paco\UnexpectedCharacterException("Unexpected character '$c', expected '{'");
	}
	if ($reader->peek() === '%') {
		return flowStatement($reader);
	}
	else {
		return interpolationStatement($reader);
	}
}

function flowStatement(\Paco\Reader $reader) {
	$reader->maybe();
	character($reader, '%', true);
	whitespace($reader);
	$token = word($reader, '/^[a-z]/', '/^[a-z]/');
	whitespace($reader);
	switch ($token) {
		case 'if':
			$reader->confirm();
			return ifStatement($reader);
		case 'switch':
			$reader->confirm();
			return switchStatement($reader);
		case 'for':
			$reader->confirm();
			return forStatement($reader);
		case 'with':
			$reader->confirm();
			return withStatement($reader);
		default:
			$reader->abort();
			throw new \Paco\NoParseException();
	}
}

function simpleTag(\Paco\Reader $reader, $allowedTagNames = null) {
	str($reader, "{%");
	whitespace($reader);
	$tagName = word($reader, '/^[a-z]/', '/^[a-z]/');
	whitespace($reader);
	str($reader, "%}");
	if (is_array($allowedTagNames) && !in_array($tagName, $allowedTagNames)) {
		throw new \Paco\UnexpectedCharacterException("Unexpected tag '$tagName', expected one of (" . implode(',', $allowedTagNames) . ")");
	}
	return $tagName;
}

function ifStatement(\Paco\Reader $reader) {
	whitespace($reader);
	$condition = expression($reader);
	whitespace($reader);
	str($reader, "%}");
	$trueBranch = body($reader);
	$tagName = simpleTag($reader, array('endif', 'else'));
	switch ($tagName) {
		case 'else':
			$falseBranch = body($reader);
			simpleTag($reader, array('endif'));
			return array("if", $condition, $trueBranch, $falseBranch);
		case 'endif':
			return array("if", $condition, $trueBranch);
	}
	throw new \Paco\ParserException("Something went wrong in inexplicable ways.");
}

function switchBranch(\Paco\Reader $reader) {
	whitespace($reader);
	$reader->maybe();
	try {
		str($reader, "{%");
		whitespace($reader);
		str($reader, "case");
		whitespace($reader);
		$compVal = expression($reader);
		whitespace($reader);
		str($reader, "%}");
		$body = body($reader);
		simpleTag($reader, 'endcase');
		$reader->confirm();
	}
	catch (\Paco\NoParseException $ex) {
		$reader->abort();
		throw new \Paco\NoParseException();
	}
	return array("case", $compVal, $body);
}

function switchStatement(\Paco\Reader $reader) {
	whitespace($reader);
	$refExpr = expression($reader);
	whitespace($reader);
	str($reader, "%}");
	$branches = many($reader, '\Paco\Parser\switchBranch');
	simpleTag($reader, 'endswitch');
	return array('switch', $refExpr, $branches);
}

function forStatement(\Paco\Reader $reader) {
	return forOrWith($reader, 'for', 'for');
}

function withStatement(\Paco\Reader $reader) {
	return forOrWith($reader, 'with', 'let');
}


function forOrWith(\Paco\Reader $reader, $forOrWith, $outHead) {
	$obj = expression($reader);
	if ($reader->peek() === ':') {
		$reader->read(1);
		whitespace($reader);
		$localname = identifier($reader);
	}
	else {
		$localname = ".";
	}
	whitespace($reader);
	str($reader, "%}");
	$body = body($reader);
	simpleTag($reader, array('end' . $forOrWith));
	return array($outHead, $obj, $localname, $body);
}

function interpolationStatement(\Paco\Reader $reader) {
	$wrap = null;
	switch ($reader->peek()) {
		case '!':
			$reader->read(1);
			break;
		case '@':
			$wrap = "urlencode";
			$reader->read(1);
			break;
		default:
			$wrap = "html";
			break;
	}
	$expr = expression($reader);
	whitespace($reader);
	character($reader, '}', true);
	if ($wrap) {
		$inner = array($wrap, $expr);
	}
	else {
		$inner = $expr;
	}
	return array("print", $inner);
}
