#!/usr/bin/env php
<?php

require_once('Exceptions.php');
require_once('Reader.php');
require_once('Parser.php');

$input = file_get_contents("php://stdin");
$reader = new \Paco\StringReader($input);
$ast = \Paco\Parser\body($reader);

echo json_encode($ast) . "\n";
