<?php

namespace Paco;

class NotImplementedException extends Exception {}

class ReaderException extends Exception {}
class ParserException extends Exception {}

class InvalidSeekOffsetException extends ReaderException {}
class UnexpectedEofException extends ReaderException {}
class ReaderStackUnderflowException extends ReaderException {}

// Non-fatal no-parse; mostly useful for speculative and LL(1) parsing, where
// recovery from a no-parse is possible.
class NoParseException extends ParserException {}
class UnexpectedCharacterException extends ParserException {}
