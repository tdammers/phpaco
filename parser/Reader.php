<?php

namespace Paco;

interface Reader {
	/**
	 * Gets the next character from the input without consuming it
	 */
	function peek();
	/**
	 * Consume $numBytes characters from the input and return them as a
	 * string.
	 */
	function read($numBytes = 1, $failOnEof = false);

	/**
	 * Return true if end-of-input is reached.
	 */
	function eof();

	// support for speculative parsing:
	function maybe();
	function confirm();
	function abort();
}

class StringReader implements Reader {
	private $str;
	private $pos;
	private $line;
	private $col;
	private $parent;

	public function __construct($str, $pos = 0, $line = 1, $col = 1, StringReader $parent = null) {
		$this->str = $str;
		$this->pos = $pos;
		$this->line = $line;
		$this->col = $col;
		$this->parent = $parent;
	}

	private function seekTo($pos) {
		$delta = $pos - $this->pos;
		$this->seek($delta);
	}

	private function seek($delta) {
		if ($delta < 0) {
			throw new InvalidSeekOffsetException();
		}
		for ($i = 0; $i < $delta; ++$i) {
			if ($this->peek() == "\n") {
				++$this->line;
				$this->col = 1;
			}
			else {
				++$this->col;
			}
			++$this->pos;
		}
	}

	public function peek() {
		if (strlen($this->str) <= $this->pos) {
			return false;
		}
		else {
			return $this->str[$this->pos];
		}
	}

	public function read($numBytes = 1, $failOnEof = false) {
		if ($this->pos + $numBytes >= strlen($this->str)) {
			if ($failOnEof) {
				throw new UnexpectedEofException();
			}
			else {
				$result = substr($this->str, $this->pos);
				$this->seekTo(strlen($this->str));
				return $result;
			}
		}
		else {
			$result = substr($this->str, $this->pos, $numBytes);
			$this->seek($numBytes);
			return $result;
		}
	}

	public function eof() {
		return ($this->pos >= strlen($this->str));
	}

	public function maybe() {
		$this->parent = new StringReader($this->str, $this->pos, $this->line, $this->col, $this->parent);
	}

	public function confirm() {
		if (!$this->parent) {
			throw new ReaderStackUnderflowException();
		}
		$oldParent = $this->parent;
		$this->parent = $oldParent->parent;
		// break circular reference 
		$oldParent->parent = null;
	}

	public function abort() {
		$this->str = $this->parent->str;
		$this->pos = $this->parent->pos;
		$this->line = $this->parent->line;
		$this->col = $this->parent->col;
		$this->confirm();
	}
}
