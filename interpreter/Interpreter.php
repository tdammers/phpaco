<?php

namespace Paco;

class UndefinedValue {}

class Interpreter {
	private $lenient = true;

	public function __construct() {
		$this->lenient = true;
	}

	public function evaluate(\Paco\Context $context, $expr) {
		if (is_array($expr)) {
			return $this->evaluateForm($context, $expr);
		}
		else {
			return $expr;
		}
	}

	public function resolve($key, $object) {
		if ($key === '.') {
			return $object;
		}
		if (is_array($object)) {
			if (isset($object[$key]))
				return $object[$key];
			else
				return new UndefinedValue();
		}
		else if (is_object($object)) {
			if (isset($object->$key))
				return $object->$key;
			else
				return new UndefinedValue();
		}
		else {
			return null;
		}
	}

	public function flatten($value) {
		if ($value instanceof UndefinedValue) {
			if ($this->lenient) {
				return '';
			}
			else {
				throw new Exception("Undefined value!");
			}
		}
		if (is_array($value)) {
			$flat = array();
			foreach ($value as $elem) {
				$flat[] = $this->flatten($elem);
			}
			return implode(' ', $flat);
		}
		return (string)$value;
	}

	public function evaluateForm(\Paco\Context $context, $expr) {
		$token = array_shift($expr);
		switch ($token) {
			case 'nop':
				return null;
			case 'getval':
				if (isset($expr[1])) {
					$object = $this->evaluate($context, $expr[1]);
				}
				else {
					$object = $context->scope;
				}
				$key = $this->evaluate($context, $expr[0]);
				$val = $this->resolve($key, $object);
				if ($val instanceof UndefinedValue)
					return $this->resolve($key, $context);
				else
					return $val;
			case 'print':
				$v = $this->evaluate($context, $expr[0]);
				echo $this->flatten($v);
				return null;
			case 'progn':
			case 'do':
				foreach ($expr as $subexpr) {
					$val = $this->evaluate($context, $subexpr);
				}
				return $val;
			case 'pair':
				return array($this->evaluate($context, $expr[0]), $this->evaluate($context, $expr[1]));
			case 'list':
				$values = array();
				foreach ($expr as $subexpr) {
					$values[] = $this->evaluate($context, $subexpr);
				}
				return $values;
			case 'alist':
				$values = array();
				foreach ($expr as $subexpr) {
					list($left, $right) = $subexpr;
					$leftVal = $this->evaluate($context, $left);
					$rightVal = $this->evaluate($context, $right);
					$values[$left] = $right;
				}
				return $values;
			case 'add': return $this->evaluate($context, $expr[0]) + $this->evaluate($context, $expr[1]);
			case 'sub': return $this->evaluate($context, $expr[0]) - $this->evaluate($context, $expr[1]);
			case 'mul': return $this->evaluate($context, $expr[0]) * $this->evaluate($context, $expr[1]);
			case 'div': return $this->evaluate($context, $expr[0]) / $this->evaluate($context, $expr[1]);
			case 'mod': return $this->evaluate($context, $expr[0]) % $this->evaluate($context, $expr[1]);
			case 'join': return $this->flatten($this->evaluate($context, $expr[0])) . $this->flatten($this->evaluate($context, $expr[1]));

			case 'and': return $this->evaluate($context, $expr[0]) && $this->evaluate($context, $expr[1]);
			case 'or': return $this->evaluate($context, $expr[0]) || $this->evaluate($context, $expr[1]);
			case 'xor':
				// PHP does not have a boolean XOR
				$a = $this->evaluate($context, $expr[0]);
				$b = $this->evaluate($context, $expr[1]);
				return ($a || $b) && !($a && $b);

			case 'not': return !$this->evaluate($context, $expr[0]);

			case 'in':
				$item = $this->evaluate($context, $expr[0]);
				$list = $this->evaluate($context, $expr[1]);
				return ((is_array($list) && in_array($item, $list)) ||
						(is_object($list) && isset($item->$list)));

			case 'denil':
				$a = $this->evaluate($context, $expr[0]);
				$b = $this->evaluate($context, $expr[1]);
				if (is_null($a) || ($a instanceof UndefinedValue))
					return $b;
				else
					return $a;

			case 'eq': return $this->evaluate($context, $expr[0]) === $this->evaluate($context, $expr[1]);
			case 'eq~': return $this->evaluate($context, $expr[0]) == $this->evaluate($context, $expr[1]);
			case 'neq': return $this->evaluate($context, $expr[0]) !== $this->evaluate($context, $expr[1]);
			case 'neq~': return $this->evaluate($context, $expr[0]) != $this->evaluate($context, $expr[1]);
			case 'lt': return $this->evaluate($context, $expr[0]) < $this->evaluate($context, $expr[1]);
			case 'gt': return $this->evaluate($context, $expr[0]) > $this->evaluate($context, $expr[1]);
			case 'lte': return $this->evaluate($context, $expr[0]) <= $this->evaluate($context, $expr[1]);
			case 'gte': return $this->evaluate($context, $expr[0]) >= $this->evaluate($context, $expr[1]);

			case 'html': return htmlspecialchars($this->flatten($this->evaluate($context, $expr[0])), ENT_QUOTES);
			case 'urlencode': return rawurlencode($this->flatten($this->evaluate($context, $expr[0])));

			case 'call':
				$f = $this->evaluate($context, $expr[0]);
				if ($f instanceof FunctionPointer) {
					array_shift($expr);
					$args = array();
					foreach ($expr as $e) {
						$args[] = $this->evaluate($context, $e);
					}
					return $f->call_array($args);
				}
				else {
					throw new NotACallableFunctionException("Tried to call something that that isn't a function");
				}

			case 'calldef':
				$defname = array_shift($expr);
				$body = $context->defs[$defname];
				return $this->evaluate($context, $body);

			case 'def':
				$defname = array_shift($expr);
				$body = array_shift($expr);
				$context->defs[$defname] = $body;
				return null;

			case 'if':
				$v = $this->evaluate($context, $expr[0]);
				if ($v) {
					return $this->evaluate($context, $expr[1]);
				}
				elseif (!empty($expr[2])) {
					return $this->evaluate($context, $expr[2]);
				}
				else {
					return null;
				}

			case 'let':
				$key = array_shift($expr);
				$value = $this->evaluate($context, array_shift($expr));
				$body = array_shift($expr);
				$outerScope = $context->scope;

				if ($key === '.') {
					$data = $value;
				}
				else {
					$data = array($key => $value);
				}
				$innerScope = new Scope($outerScope, $data);
				$context->scope = $innerScope;
				$result = $this->evaluate($context, $body);
				$context->scope = $outerScope;
				return $result;

			case 'map':
			case 'for':
				$keyname = array_shift($expr);
				$localname = array_shift($expr);
				$iteree = $this->evaluate($context, array_shift($expr));
				$body = array_shift($expr);
				$outerScope = $context->scope;

				foreach ($iteree as $key => $value) {
					if ($localname === '.') {
						$data = $value;
					}
					else {
						$data = array($localname => $value);
					}
					$innerScope = new Scope($outerScope, $data);
					if ($keyname !== null) {
						$innerScope->$keyname = $key;
					}
					$context->scope = $innerScope;
					$result[] = $this->evaluate($context, $body);
				}
				$context->scope = $outerScope;
				return $result;

			case 'switch':
				$arg = $this->evaluate($context, array_shift($expr));
				$defbranch = null;
				foreach ($expr as $branch) {
					if ($branch[0] === 'case') {
						$cond = $this->evaluate($context, $branch[1]);
						if ($cond == $arg) {
							return $this->evaluate($context, $branch[2]);
						}
					}
					elseif ($branch[0] === 'default') {
						$defbranch = $branch[1];
					}
				}
				if ($defbranch)
					return $this->evaluate($context, $defbranch);
				return null;

			default:
				throw new InvalidOpcodeException('Invalid opcode "' . $token . '"');
		}
	}
}
