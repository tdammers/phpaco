<?php

namespace Paco;

class FunctionPointer {
	private $callable;

	public function __construct($callable) {
		$this->callable = $callable;
	}

	public function call_array($args) {
		return call_user_func_array($this->callable, $args);
	}

	public function __toString() {
		return '---function---';
	}
}

class Scope {
	private $values;
	private $parent;

	public function __construct($parent = null, $values = null) {
		if (isset($values))
			$this->values = $values;
		else
			$this->values = null;
		$this->parent = $parent;
	}

	public function __get($key) {
		if (empty($key) || $key === '.') {
			return $this;
		}
		if (isset($this->values[$key]))
			return $this->values[$key];
		if (is_array($this->parent)) {
			return $this->parent[$key];
		}
		if (is_object($this->parent)) {
			if (isset($this->parent->$key)) {
				return $this->parent->$key;
			}
			if (is_callable(array($this->parent, $key))) {
				return new FunctionPointer($this->parent, $key);
			}
		}
	}

	public function __set($key, $val) {
		if (!is_array($this->values))
			$this->values = array();
		$this->values[$key] = $val;
	}

	public function __toString() {
		if (is_object($this->values))
			return '--- scope ---';

		if (is_array($this->values))
			return implode(' ', $this->values);

		if (is_null($this->values))
			return (string)$this->parent;

		return (string)$this->values;
	}

	public function __isset($key) {
		if (empty($key) || $key === '.') {
			return true;
		}
		if (isset($this->values[$key]))
			return true;
		if (is_array($this->parent)) {
			return isset($this->parent[$key]);
		}
		if (is_object($this->parent)) {
			if (isset($this->parent->$key)) {
				return true;
			}
			if (is_callable(array($this->parent, $key))) {
				return true;
			}
		}
		return false;
	}
}
