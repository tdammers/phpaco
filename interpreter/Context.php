<?php

namespace Paco;

class Context {
	public $scope;
	public $defs;

	public function __construct($data) {
		if ($data instanceof Scope)
			$this->scope = $data;
		else
			$this->scope = new Scope($data);
		$this->defs = array();
	}

	public function def($name, $body) {
		$this->defs[$name] = $body;
	}
};
