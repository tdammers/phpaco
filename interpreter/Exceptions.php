<?php

namespace Paco;

class InterpreterException extends Exception {}

class InvalidOpcodeException extends InterpreterException {}
class NotACallableFunctionException extends InterpreterException {}
